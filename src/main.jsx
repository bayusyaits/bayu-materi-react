import React from 'react'
import ReactDOM from 'react-dom/client'
import {BrowserRouter as Router} from 'react-router-dom';
import App from './App'
import './index.css'

const element = (
  <Router>
    <App />
  </Router>
);

const container = document.getElementById('root');
ReactDOM.createRoot(container).render(element)


