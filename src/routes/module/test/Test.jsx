import React, { useEffect, useState } from "react";
import Modal from "react-modal";

const style = {
  wrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    marginBottom: 10
  },
  row: { display: "flex", flexDirection: "row", alignItems: "flex-start" },
  result: { margin: "15px 0" }
};
const resolveTimeout = (value, delay) => {
  return new Promise((resolve) => setTimeout(() => resolve(value), delay));
};
const rejectWithDelay = (msg) =>
  new Promise((resolve, reject) => setTimeout(() => reject(msg), 1000));
const userData =
  '[{"name":"John Doe","email":"john.doe@mail.com","type":"basic","number":1},{"name":"Johnny Deep","email":"j.deep@mail.com","type":"basic","number":3},{"name":"Nick Morgan","email":"nickmorgan@mail.com","type":"vip","number":6},{"name":"Martin J","email":"martin.john@mail.com","type":"basic","number":2},{"name":"Martin Wel","email":"martinw@mail.com","type":"vip","number":8},{"name":"Johnny Christian","email":"j.christ@mail.com","type":"vip","number":4},{"name":"Julian","email":"j.jo@mail.com","type":"vip","number":2},{"name":"Jason McMilan","email":"mcmilan@mail.com","type":"basic","number":9}]';

// Helper functions
const getUsers = async () => await resolveTimeout(JSON.parse(userData), 10).then((res)=>res).catch((err) => rejectWithDelay(err));

const UserComponent = ({ users }) => {
  if ( users && users.length) {
    return (<>
      <ul>
        {users.map((el) => {
          return (<li style={style.row} key={el.email}>
            <p>{el.name}</p>
            <p>{el.email}</p>
            <p>{el.basic}</p>
            <p>{el.number}</p>
          </li>)
        })}
      </ul>
    </>)
  } else {
    return (<>Not found</>)
  }
};

const TestComponent = () => {
  const [isOpen, setIsOpen] = useState(false);
  const handleOpenModal = (data) => {
    setIsOpen(true)
  }
  const ShowModal = () => {
    const FilterField = () => (
      <div style={style.row}>
        <select>
          <option value=''>Any</option>
        </select>
        <input type="text" placeholder='value' />
      </div>
    )
    const handleClose = () => {
      setIsOpen(false)
    }
    return (
      <Modal
        isOpen={isOpen}
        onRequestClose={handleClose}
        contentLabel="Example Modal"
      >
        <h2>Filter</h2>
        <button onClick={() => handleClose()}>close</button>   
        <FilterField/>     
      </Modal>)
  }
  const [users, setUsers] = useState({})
  useEffect( async () => {
    await getUsers().then((res) => {
      setUsers(res)
    })
  }, [])
  return (
    <>
      <span style={style.result}>USERS:</span>
      <UserComponent users={users}/>
      <ShowModal/>
      <button type="button" onClick={() => handleOpenModal()}>Filter</button>
    </>
  );
};

export default TestComponent;