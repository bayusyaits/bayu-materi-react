import React from "react";
import ModuleThirtySixthFunctionView from "./ModuleThirtySixthFunctionView";

function ModuleThirtySixthFunctionContainer() {
  return (
    <div style={{ height: '100%' }}> 
     <ModuleThirtySixthFunctionView render={position => ( 
   <h1>Mouse position: ({position.x}, 
   {position.y})</h1> )} /> 
     </div>); 
}

export default ModuleThirtySixthFunctionContainer;
