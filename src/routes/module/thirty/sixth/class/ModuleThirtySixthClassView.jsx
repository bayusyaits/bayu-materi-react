import React, { Component } from 'react';
function withLogger(Component) {
  return class extends React.Component {
    constructor(props) {
      super(props)
      this.state = {value: ''}
    }
    componentWillMount() {
      console.log('Component will mount!');
      this.setState({value: 'Will Mount'})
    }
    componentWillUnmount() {
      console.log('Component will unmount!');
    }

    render() {
      return <Component 
        value={this.state.value}
      {...this.props} />;
    }
  };
 }
class ModuleThirtySixthClassView extends Component {
  constructor(props) { 
    super(props);  
    this.value = props.value
  } 
  render() {
    return <div>Hello World! {this.value}</div>;
 }
}

export default withLogger(ModuleThirtySixthClassView);