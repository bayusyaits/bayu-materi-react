import React, { PureComponent } from "react";
import ModuleThirtySixthClassView from "./ModuleThirtySixthClassView";

export default class ModuleThirtySixthClassContainer extends PureComponent {
  render() {
    return (
 	   <ModuleThirtySixthClassView />
    );
  }
}