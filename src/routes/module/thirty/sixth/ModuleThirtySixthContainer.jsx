import React from "react";
import ModuleThirtySixthView from "./ModuleThirtySixthView";
import ModuleThirtySixthClass from "./class";
import ModuleThirtySixthFunction from "./function";
import { useParams } from "react-router-dom";

function ModuleThirtySixthContainer() {
  const {
    name
  } = useParams()
  const s = name ? name.toLocaleLowerCase() : ''
  let Component = ModuleThirtySixthView
  if (s) {
    switch (s) {
      case 'class': 
        Component = ModuleThirtySixthClass
        break;
      case 'function': 
        Component = ModuleThirtySixthFunction
        break; 
      default: 
        Component = ModuleThirtySixthView
        break;
    }
  }

  return (
      <Component
      />
  );
}

export default ModuleThirtySixthContainer;
