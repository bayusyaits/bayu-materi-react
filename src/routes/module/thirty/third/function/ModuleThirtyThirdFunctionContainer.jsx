import React from "react";
import ModuleThirtyThirdFunctionView from "./ModuleThirtyThirdFunctionView";

function ModuleThirtyThirdFunctionContainer() {
  return (
    <div style={{ height: '100%' }}> 
      <ModuleThirtyThirdFunctionView render={position => ( 
      <h1>Mouse position: ({position.x}, {position.y})</h1> )} /> 
    </div>); 
}

export default ModuleThirtyThirdFunctionContainer;
