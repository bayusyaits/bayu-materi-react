import React, {useState} from 'react'
function ModuleThirtyThirdFunctionView({ render }) { 
  const [position, setPosition] = useState({ x:   
  0, y: 0 }); 

  function handleMouseMove(event) { 
    setPosition({ x: event.clientX, y:  
    event.clientY });
  } 
  return (<div style={{ height: '100%' }} 
    onMouseMove={handleMouseMove}> 
    {render(position)} </div> ); 
}
export default ModuleThirtyThirdFunctionView
