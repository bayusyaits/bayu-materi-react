import React, { PureComponent } from "react";
import ModuleThirtyThirdClassView from "./ModuleThirtyThirdClassView";

export default class ModuleThirtyThirdClassContainer extends PureComponent {
  render() {
    return (
	  <div>
 	   <ModuleThirtyThirdClassView render={(mouse) => 
      (
        <h1>The mouse position is ({mouse.x}, {mouse.y})</h1>
      )}/>
    </div>
    );
  }
}