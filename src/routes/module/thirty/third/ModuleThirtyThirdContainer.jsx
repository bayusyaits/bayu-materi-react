import React from "react";
import ModuleThirtyThirdView from "./ModuleThirtyThirdView";
import ModuleThirtyThirdClass from "./class";
import ModuleThirtyThirdFunction from "./function";
import { useParams } from "react-router-dom";

function ModuleThirtyThirdContainer() {
  const {
    name
  } = useParams()
  const s = name ? name.toLocaleLowerCase() : ''
  let Component = ModuleThirtyThirdView
  if (s) {
    switch (s) {
      case 'class': 
        Component = ModuleThirtyThirdClass
        break;
      case 'function': 
        Component = ModuleThirtyThirdFunction
        break; 
      default: 
        Component = ModuleThirtyThirdView
        break;
    }
  }

  return (
      <Component
      />
  );
}

export default ModuleThirtyThirdContainer;
