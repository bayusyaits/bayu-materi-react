import React from 'react'
function ModuleThirtySecondView({
}) {
  const StudentList = (props) => {
    const list = props.list;
    const listItems = list.map((item) =>
      <li key={item.id}><p>{item.name}</p></li>
    );
    return (<ul>{listItems}</ul>);
  }
  
  const students = [
    { id: 1, name: 'Bayu' },
    { id: 2, name: 'Zaki' },
    { id: 3, name: 'Alia' }
  ];
  
  return (
    <div>
      <h1>Daftar Pelajar</h1>
      <StudentList list={students}/>
    </div>
  )
}

export default ModuleThirtySecondView
