import React, { useState } from "react";

const NameComponent = () => {
 const [name, setName] = useState('');
 const handleSubmit = (event) => {
   event.preventDefault();
   alert(`Halo, ${name}!`); 
 };
 
 const handleNameChange = (event) => {
   setName(event.target.value); 
 };
 
 return (<form 
  onSubmit={handleSubmit}>
   <label> Name: 
      <input 
        type="text"
        value={name} 
        onChange={handleNameChange} 
      />
     </label>
     <button type="submit">Submit</button>
  </form>);
}


export default NameComponent
