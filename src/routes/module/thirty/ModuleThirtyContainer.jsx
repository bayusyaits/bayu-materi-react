import React from "react";
import ModuleThirtyView from "./ModuleThirtyView";
import ModuleThirtyFirst from "./first";
import ModuleThirtySecond from "./second";
import ModuleThirtyThird from "./third";
import ModuleThirtyFourth from "./fourth";
import ModuleThirtyFifth from "./fifth";
import ModuleThirtySixth from "./sixth";
import ModuleThirtySeventh from "./seventh";
import ModuleThirtyEighth from "./eighth";
import { useParams } from "react-router-dom";

function ModuleThirtyContainer() {
  const {
    slug
  } = useParams()
  let Component = ModuleThirtyView
  const s = slug ? slug.toLocaleLowerCase() : ''
  if (s) {
    switch (s) {
      case 'first': 
        Component = ModuleThirtyFirst
        break;
      case 'second': 
        Component = ModuleThirtySecond
        break; 
      case 'third': 
        Component = ModuleThirtyThird
        break; 
      case 'fourth': 
        Component = ModuleThirtyFourth
        break; 
      case 'fifth': 
        Component = ModuleThirtyFifth
        break; 
      case 'sixth': 
        Component = ModuleThirtySixth
        break; 
      case 'seventh': 
        Component = ModuleThirtySeventh
        break; 
      case 'eighth': 
        Component = ModuleThirtyEighth
        break; 
      default:
        Component = ModuleThirtyView;
        break;
    }
  }
  return (
      <Component
      />
  );
}

export default ModuleThirtyContainer;
