import React from "react";
import { useParams } from "react-router-dom";
import ModuleThirtyFourthView from "./ModuleThirtyFourthView";
import ModuleThirtyFourthCallback from "./callback";
import ModuleThirtyFourthCreateRef from "./createRef";
import ModuleThirtyFourthUseRef from "./useRef";

function ModuleThirtyFourthContainer() {
  const {
    name
  } = useParams()
  const s = name ? name.toLocaleLowerCase() : ''
  let Component = ModuleThirtyFourthView
  if (s) {
    switch (s) {
      case 'callback': 
        Component = ModuleThirtyFourthCallback
        break;
      case 'useref': 
        Component = ModuleThirtyFourthUseRef
        break; 
      case 'createref': 
        Component = ModuleThirtyFourthCreateRef
        break;
      default: 
        Component = ModuleThirtyFourthView
        break;
    }
  }
  return (
      <Component
      />
  );
}

export default ModuleThirtyFourthContainer;
