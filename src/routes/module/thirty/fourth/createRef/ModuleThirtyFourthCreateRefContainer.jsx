import React from "react";
import ModuleThirtyFourthCreateRefView from "./ModuleThirtyFourthCreateRefView";

function ModuleThirtyFourthCreateRefContainer() {
  return (
      <ModuleThirtyFourthCreateRefView
      />
  );
}

export default ModuleThirtyFourthCreateRefContainer;
