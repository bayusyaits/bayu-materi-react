import React from 'react'
export default class ExampleComponent extends React.Component {  
  constructor(props) { 
    super(props);  
    this.textInput = React.createRef();
  } 
  focusTextInput = () => {   
    const value = this.textInput.current.value
    this.textInput.current.focus(); 
    if (value) {
      this.textInput.current.value = value.toLowerCase()
    }
  } 
  render() { 
    return (<div>
      <input type="text" 
             ref={this.textInput} /> 
      <button onClick={this.focusTextInput}>
      Focus</button></div>
    ); 
  } 
}