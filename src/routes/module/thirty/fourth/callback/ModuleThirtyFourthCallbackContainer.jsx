import React from "react";
import ModuleThirtyFourthCallbackView from "./ModuleThirtyFourthCallbackView";

function ModuleThirtyFourthCallbackContainer() {
  return (
      <ModuleThirtyFourthCallbackView
      />
  );
}

export default ModuleThirtyFourthCallbackContainer;
