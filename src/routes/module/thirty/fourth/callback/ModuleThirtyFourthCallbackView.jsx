import {useRef, useState} from 'react'
function ModuleThirtyFourthView({
}) {
  const inputRef = useRef(null);
  const [value, setValue] = useState('')
   const handleButtonClick = () => {
     const inputValue = inputRef.current.value;
     setValue(inputValue)
     inputRef.current.focus() 
   };

   return (
     <div>
        <p>Result: {value}</p>
       <input type="text" ref={inputRef} />
       <button 
         onClick={handleButtonClick}>     
         Submit</button>
     </div>
  );

}

export default ModuleThirtyFourthView
