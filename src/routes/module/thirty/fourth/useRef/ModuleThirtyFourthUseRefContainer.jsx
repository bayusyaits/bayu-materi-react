import React from "react";
import ModuleThirtyFourthUseRefView from "./ModuleThirtyFourthUseRefView";

function ModuleThirtyFourthUseRefContainer() {
  return (
      <ModuleThirtyFourthUseRefView
      />
  );
}

export default ModuleThirtyFourthUseRefContainer;
