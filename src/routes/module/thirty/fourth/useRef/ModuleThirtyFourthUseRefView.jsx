import { useRef } from 'react';
function ModuleThirtyFourthUseRefView({
}) {
  const inputRef = useRef(null);
  const handleClick = () => {
    const inputValue = inputRef.current.value;
    alert('Value: '+ inputValue)  
  }
  return (<div>
    <input type="text" ref={inputRef} />
    <button onClick={handleClick}>
      Submit
    </button>
  </div>);
}

export default ModuleThirtyFourthUseRefView
