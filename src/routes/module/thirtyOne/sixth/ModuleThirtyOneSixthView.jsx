import { useState} from "react";
import {LanguageContext, Language} from "./ModuleThirtyOneSixthContext";

function ModuleThirtyOneSixthView() {
 const [lang, setLang] = useState("en");
 const value = { lang, setLang };
 return (
   <LanguageContext.Provider value={value}>
     <h1>{`${lang === 'id' ? 'Bahasa' : 'English'}!`}</h1>
     <Language />
   </LanguageContext.Provider>
 );
}

export default ModuleThirtyOneSixthView
