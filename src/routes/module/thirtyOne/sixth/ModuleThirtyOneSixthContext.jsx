import { createContext, useContext } from "react";
export const LanguageContext = createContext();

export const Language = () => {
 const {lang, setLang} = useContext(LanguageContext);
 const handleClick = () => {
   if (lang === 'en') {
     setLang('id')
   } else {
     setLang('en')
   }
 }
 return (
   <>
     <button type="button" onClick={handleClick}>
       {lang === 'en' ? 'Ubah ke ID' : 'Change to EN'}
     </button>
   </>
 );
}
