import React from "react";
import ModuleThirtyOneSixthView from "./ModuleThirtyOneSixthView";
import ModuleThirtyOneSixthClass from "./class";
import ModuleThirtyOneSixthFunction from "./function";
import { useParams } from "react-router-dom";

function ModuleThirtyOneSixthContainer() {
  const {
    name
  } = useParams()
  const s = name ? name.toLocaleLowerCase() : ''
  let Component = ModuleThirtyOneSixthView
  if (s) {
    switch (s) {
      case 'class': 
        Component = ModuleThirtyOneSixthClass
        break;
      case 'function': 
        Component = ModuleThirtyOneSixthFunction
        break; 
      default: 
        Component = ModuleThirtyOneSixthView
        break;
    }
  }

  return (
      <Component
      />
  );
}

export default ModuleThirtyOneSixthContainer;
