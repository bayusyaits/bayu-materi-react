import React from "react";
import ModuleThirtyOneView from "./ModuleThirtyOneView";
import ModuleThirtyOneFirst from "./first";
import ModuleThirtyOneSecond from "./second";
import ModuleThirtyOneThird from "./third";
import ModuleThirtyOneFourth from "./fourth";
import ModuleThirtyOneFifth from "./fifth";
import ModuleThirtyOneSixth from "./sixth";
import ModuleThirtyOneSeventh from "./seventh";
import ModuleThirtyOneEighth from "./eighth";
import { useParams } from "react-router-dom";

function ModuleThirtyOneContainer() {
  const {
    slug
  } = useParams()
  let Component = ModuleThirtyOneView
  const s = slug ? slug.toLocaleLowerCase() : ''
  if (s) {
    switch (s) {
      case 'first': 
        Component = ModuleThirtyOneFirst
        break;
      case 'second': 
        Component = ModuleThirtyOneSecond
        break; 
      case 'third': 
        Component = ModuleThirtyOneThird
        break; 
      case 'fourth': 
        Component = ModuleThirtyOneFourth
        break; 
      case 'fifth': 
        Component = ModuleThirtyOneFifth
        break; 
      case 'sixth': 
        Component = ModuleThirtyOneSixth
        break; 
      case 'seventh': 
        Component = ModuleThirtyOneSeventh
        break; 
      case 'eighth': 
        Component = ModuleThirtyOneEighth
        break; 
      default:
        Component = ModuleThirtyOneView;
        break;
    }
  }
  return (
      <Component
      />
  );
}

export default ModuleThirtyOneContainer;
