import useFetch from "./useFetch";
const ModuleThirtyOneEighthView = () => {
  const [data] = useFetch("https://jsonplaceholder.typicode.com/comments");
  return (
   <>
     {data &&
       data.map((item) => {
         return <p key={item.id}>{item.body}</p>;
       })}
   </>
 );
};
export default ModuleThirtyOneEighthView

