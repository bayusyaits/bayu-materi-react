import {useState} from 'react'
function ModuleThirtyOneSecondView({
}) {
  const [count, setCount] = useState(0);

 function handleClick() {
   setCount(count + 1);
 }

 return (
   <div>
     <p>Clicked {count}</p>
     <button onClick={handleClick}>Click</button>
   </div>
 );
}

export default ModuleThirtyOneSecondView
