import { useMemo } from 'react';
const ModuleThirtyOneFifthMemoView = (props) => {
  const sum = useMemo(() => () => {
    if (props.arr) {
    return props.arr.reduce((total, num) => total + num, 0);
    }
  }, [props.arr]);
 
  return (
    <div>
      <p>Sum: {sum()}</p>
    </div>
  );
 
 };
 
export default ModuleThirtyOneFifthMemoView
