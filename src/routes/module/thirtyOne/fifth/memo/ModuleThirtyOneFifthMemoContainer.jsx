import React from "react";
import ModuleThirtyOneFifthMemoView from "./ModuleThirtyOneFifthMemoView";

function ModuleThirtyOneFifthMemoContainer(props) {
  return (
      <ModuleThirtyOneFifthMemoView
        {...props}
      />
  );
}

export default ModuleThirtyOneFifthMemoContainer;
