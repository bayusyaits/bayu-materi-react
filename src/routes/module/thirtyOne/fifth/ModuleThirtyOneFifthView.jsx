import { useState } from "react";
import ContohMemo from './memo'
const ModuleThirtyOneFifthView = () => {
  const [count, setCount] = useState([])
  const handleClick = () => {
    setCount((arr) => [...arr, 1])
  }
  return (<>
     <ContohMemo arr={count}/>
     <div><button type="button" onClick={handleClick}>Tambah</button></div>
    </>
  ); 
}


export default ModuleThirtyOneFifthView
