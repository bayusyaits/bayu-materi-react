import {useState, useCallback} from 'react'
import Data from './data'
function ModuleThirtyOneFourthView({
}) {
  const [count, setCount] = useState(0);
  const [data, setData] = useState([]);
  const increment = () => {
    setCount((c) => c + 1);
  };
  const addData = useCallback(() => {
    setData((t) => [...t, "Data Baru"]);
  }, [data]);
  return (
    <>
      <Data data={data} addData={addData} />
      <div>
        Hitung: {count}
        <button onClick={increment}>+</button>
      </div>
    </>
  );
}

export default ModuleThirtyOneFourthView
