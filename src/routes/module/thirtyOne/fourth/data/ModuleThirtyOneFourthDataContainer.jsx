import React from "react";
import ModuleThirtyOneFourthDataView from "./ModuleThirtyOneFourthDataView";

function ModuleThirtyOneFourthDataContainer(props) {
  return (
      <ModuleThirtyOneFourthDataView
        {...props}
      />
  );
}

export default ModuleThirtyOneFourthDataContainer;
