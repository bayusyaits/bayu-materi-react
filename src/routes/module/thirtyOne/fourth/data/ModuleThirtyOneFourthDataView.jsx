const ModuleThirtyOneFourthDataView = ({ data, addData }) => {
  return (
    <>
      <h2>Data</h2>
      {data.map((el, key) => {
        return <p style={{
            marginTop: '5px',
            marginBottom: '5px'
        }} key={key}>{el}</p>;
      })}
      <button onClick={addData}>Add</button>
    </>
  );
 };
 
export default ModuleThirtyOneFourthDataView
