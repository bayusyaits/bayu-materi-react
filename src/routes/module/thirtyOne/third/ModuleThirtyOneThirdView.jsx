import React, {useEffect, useState} from 'react'
function ModuleThirtyOneThirdView({
}) {
  const [count, setCount] = useState(0);
  useEffect(() => {
    document.title = `Total klik ${count}`;
    console.log("count update")
    return () => {
      console.log("count update or unmount")
    }
  }, [count]);
  useEffect(() => {
    console.log("mount")
    return () => {
      console.log("unmount")
      setCount(0)
    }
  }, []);
  useEffect(() => 
    console.log("any update") 
  );
  return (
    <div>
      <p>Total klik {count}</p>
      <button onClick={() => setCount(count + 1)}>
        Tambah
      </button>
    </div>
  );

}

export default ModuleThirtyOneThirdView
