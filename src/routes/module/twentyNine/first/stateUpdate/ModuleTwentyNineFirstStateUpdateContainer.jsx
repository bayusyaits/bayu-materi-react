import React, { PureComponent } from "react";
import ModuleTwentyNineFirstStateUpdateView from "./ModuleTwentyNineFirstStateUpdateView";

export default class ModuleTwentyNineFirstStateUpdateContainer extends PureComponent {
  render() {
    return (
 	   <ModuleTwentyNineFirstStateUpdateView />
    );
  }
}