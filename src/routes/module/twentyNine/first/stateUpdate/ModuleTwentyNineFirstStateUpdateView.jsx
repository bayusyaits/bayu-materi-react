import React from "react";

class ModuleTwentyNineFirstStateUpdateView extends React.Component { 
  constructor() {
    super(); 
    this.state = {
      counter: 0
    }
  }
  increment() {
    this.setState({
      counter: this.state.counter + 1
    })
  }
  decrement() {
    this.setState({
      counter: this.state.counter - 1
    })
  }
  render() {
    return (<div>
      <button onClick={() => this.increment()} type="button">+</button>
      <span style={{marginLeft: "10px", marginRight: "10px"}}>{this.state.counter}</span>
      <button onClick={() => this.decrement()} type="button">-</button>
    </div>);
  }
 } 
export default ModuleTwentyNineFirstStateUpdateView;



