import React from "react";

class ModuleTwentyNineFirstStateReadView extends React.Component { 
 constructor(props) {
   super(props); 
   this.state = {
     title: 'Belajar Menggunakan State Dalam Class Component'
   }
 }
 render() {
   return (<h2>{this.state.title}</h2>
   );
 }
}
export default ModuleTwentyNineFirstStateReadView;




