import React, { PureComponent } from "react";
import ModuleTwentyNineFirstStateReadView from "./ModuleTwentyNineFirstStateReadView";

export default class ModuleTwentyNineFirstStateReadContainer extends PureComponent {
  render() {
    return (
 	   <ModuleTwentyNineFirstStateReadView />
    );
  }
}