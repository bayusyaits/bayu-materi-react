import React from "react";
import ModuleTwentyNineFirstView from "./ModuleTwentyNineFirstView";
import ModuleTwentyNineFirstStateRead from "./stateRead";
import ModuleTwentyNineFirstGetTitle from "./getTitle";
import ModuleTwentyNineFirstStateUpdate from "./stateUpdate";
import { useParams } from "react-router-dom";

function ModuleTwentyNineFirstContainer() {
  const {
    name
  } = useParams()
  const s = name ? name.toLocaleLowerCase() : ''
  let Component = ModuleTwentyNineFirstView
  if (s) {
    switch (s) {
      case 'get-title': 
        Component = ModuleTwentyNineFirstGetTitle
        break; 
      case 'state-read': 
        Component = ModuleTwentyNineFirstStateRead
        break;
      case 'state-update': 
        Component = ModuleTwentyNineFirstStateUpdate
        break; 
      default: 
        Component = ModuleTwentyNineFirstView
        break;
    }
  }

  return (
      <Component
      />
  );
}

export default ModuleTwentyNineFirstContainer;
