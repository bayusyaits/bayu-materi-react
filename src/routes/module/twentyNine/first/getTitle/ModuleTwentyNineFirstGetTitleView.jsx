import React, { Component } from 'react';
class ModuleTwentyNineFirstStateGetTitleView extends Component {
  constructor() {
    super(); 
    this.state = {
      title: 'Belajar Menggunakan State Dalam Function'
    }
  }
  getTitle() {
    return this.state.title
  }
  render() {
    return (<h2>{this.getTitle()}</h2>
    );
  }
}

export default ModuleTwentyNineFirstStateGetTitleView;