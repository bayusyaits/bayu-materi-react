import React, { PureComponent } from "react";
import ModuleTwentyNineFirstGetTitleView from "./ModuleTwentyNineFirstGetTitleView";

export default class ModuleTwentyNineFirstGetTitleContainer extends PureComponent {
  render() {
    return (
 	   <ModuleTwentyNineFirstGetTitleView />
    );
  }
}