import React from "react";
import { useParams } from "react-router-dom";
import ModuleTwentyNineFourthView from "./ModuleTwentyNineFourthView";
import ModuleTwentyNineFourthMap from "./map";
import ModuleTwentyNineFourthCount from "./count";
import ModuleTwentyNineFourthPassingChild from "./passingChild";
import ModuleTwentyNineFourthFunctionChildren from "./functionChildren";
import ModuleTwentyNineFourthToArray from "./toArray";
import ModuleTwentyNineFourthForEach from "./forEach";
import ModuleTwentyNineFourthOnly from "./only";
import ModuleTwentyNineFourthHoc from "./hoc";

function ModuleTwentyNineFourthContainer() {
  const {
    name
  } = useParams()
  const s = name ? name.toLocaleLowerCase() : ''
  let Component = ModuleTwentyNineFourthView
  if (s) {
    switch (s) {
      case 'map': 
        Component = ModuleTwentyNineFourthMap
        break;
      case 'count': 
        Component = ModuleTwentyNineFourthCount
        break; 
      case 'passing-child': 
        Component = ModuleTwentyNineFourthPassingChild
        break;
      case 'function-children': 
        Component = ModuleTwentyNineFourthFunctionChildren
        break;
      case 'to-array': 
        Component = ModuleTwentyNineFourthToArray
        break;
      case 'for-each': 
        Component = ModuleTwentyNineFourthForEach
        break;
      case 'only': 
        Component = ModuleTwentyNineFourthOnly
        break;
      case 'hoc': 
        Component = ModuleTwentyNineFourthHoc
        break;
      default: 
        Component = ModuleTwentyNineFourthView
        break;
    }
  }
  return (
      <Component
      />
  );
}

export default ModuleTwentyNineFourthContainer;
