export const withTitle = (Component) => (props) => {
    const title = "Belajar React";
    return (<Component title={title} {...props} />);
};