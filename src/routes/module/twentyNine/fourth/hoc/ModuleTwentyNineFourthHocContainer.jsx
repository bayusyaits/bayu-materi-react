import React from "react";
import ModuleTwentyNineFourthHocView from "./ModuleTwentyNineFourthHocView";

function ModuleTwentyNineFourthHocContainer() {
  return (
      <ModuleTwentyNineFourthHocView
      />
  );
}

export default ModuleTwentyNineFourthHocContainer;
