import { withTitle } from "./withTitle";
const ModuleTwentyNineFourthHocView = (props) => {
 return (
   <div className="App">
     <h1>Halaman: {props.title}</h1>
   </div>
 );
};

export default (withTitle(ModuleTwentyNineFourthHocView));
