import React from "react";
import ModuleTwentyNineFourthPassingChildView from "./ModuleTwentyNineFourthPassingChildView";

function ModuleTwentyNineFourthPassingChildContainer() {
  return (
      <ModuleTwentyNineFourthPassingChildView
      />
  );
}

export default ModuleTwentyNineFourthPassingChildContainer;
