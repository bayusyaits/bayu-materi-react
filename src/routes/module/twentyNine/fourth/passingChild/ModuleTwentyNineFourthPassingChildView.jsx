function ItemComponent(props) {
  return (
    <div className="item">
      <h1>{props.title}</h1>
      <div>{props.children}</div>
    </div>
  );
 }
 function ModuleTwentyNineFourthPassingChildView() {
  return (
    <ItemComponent title={'Hello World'}>
      <p>Ini Item.</p>
    </ItemComponent>
  );
 } 

export default ModuleTwentyNineFourthPassingChildView
