import React from 'react';

function ChildrenComponent(props) {
  return (
    <ul>
      {React.Children.map(props.children, child => (
        <li>{child}</li>
      ))}
    </ul>
  );
 }
 function ModuleTwentyNineFourthMapView() {
  return (
    <ChildrenComponent>
      <p>Item 1</p>
      <p>Item 2</p>
    </ChildrenComponent>
  );
 }
 
export default ModuleTwentyNineFourthMapView
