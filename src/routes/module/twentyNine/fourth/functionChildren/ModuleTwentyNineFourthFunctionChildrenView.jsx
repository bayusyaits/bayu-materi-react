function EmployeeComponent(props) {
  return (
    <div className="item">
      {props.render()}
    </div>
  );
}

function ModuleTwentyNineFourthFunctionChildrenView() {
  const employee = [{
    name: 'John',
    nik: '401818111'
   },{
    name: 'Jack',
    nik: '301818113'
  }]
  return (<EmployeeComponent render={data =>(
     <ul>
     {employee.map((item, index) => (
       <li key={index}>{item.name} ({item.nik})</li>
     ))}
     </ul>)} 
  />);
}


export default ModuleTwentyNineFourthFunctionChildrenView
