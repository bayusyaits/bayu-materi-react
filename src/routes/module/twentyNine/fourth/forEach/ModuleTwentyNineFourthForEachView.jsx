import React, {useEffect} from 'react';
const FirstComponent = ({ name }) => <li>{name}</li>;
FirstComponent.displayName = 'FirstComponent';
const SecondComponent = ({ name }) => <li>{name}</li>;
SecondComponent.displayName = 'SecondComponent';
function ParentComponent(props) {
 useEffect(() => {
   React.Children.forEach(props.children, child => {
     console.log('name =', child.type.displayName);
   })
 }, [])
 return (<ul>{props.children}</ul>);
}
function ModuleTwentyNineFourthForEachView() {
 return (
   <ParentComponent>
     <FirstComponent name='John' />
     <SecondComponent name='Jack' />
   </ParentComponent>
 );
}

export default ModuleTwentyNineFourthForEachView
