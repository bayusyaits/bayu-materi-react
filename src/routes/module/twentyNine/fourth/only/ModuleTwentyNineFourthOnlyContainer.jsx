import React from "react";
import ModuleTwentyNineFourthOnlyView from "./ModuleTwentyNineFourthOnlyView";

function ModuleTwentyNineFourthOnlyContainer() {
  return (
      <ModuleTwentyNineFourthOnlyView
      />
  );
}

export default ModuleTwentyNineFourthOnlyContainer;
