import React from 'react';
const FirstComponent = ({ name }) => <li>{name}</li>;
const ParentComponent = (props) => {
  const el = React.Children.only(props.children);
  return (
    <div>
      {el}
    </div>
  )
}

function ModuleTwentyNineFourthOnlyiew() {
return (
  <ParentComponent>
    <FirstComponent name='John' />
  </ParentComponent>
);
}

export default ModuleTwentyNineFourthOnlyiew
