import React from "react";
import ModuleTwentyNineFourthCountView from "./ModuleTwentyNineFourthCountView";

function ModuleTwentyNineFourthCountContainer() {
  return (
      <ModuleTwentyNineFourthCountView
      />
  );
}

export default ModuleTwentyNineFourthCountContainer;
