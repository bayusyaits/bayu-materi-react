import React from 'react';
const FirstComponent = ({ name }) => <li>{name}</li>;
const SecondComponent = ({ name }) => <li>{name}</li>;
const ParentComponent = (props) => {
   const count = React.Children.count(props.children);
   return (
     <div>
       <p>Terdapat {count} element.</p>
       {props.children}
     </div>
   )
}

function ModuleTwentyNineFourthCountView() {
 return (
   <ParentComponent>
     <FirstComponent name='John' />
     <SecondComponent name='Jack' />
   </ParentComponent>
 );
}

export default ModuleTwentyNineFourthCountView
