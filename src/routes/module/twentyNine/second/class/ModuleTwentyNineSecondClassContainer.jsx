import React, { PureComponent } from "react";
import ModuleTwentyNineSecondClassView from "./ModuleTwentyNineSecondClassView";

export default class ModuleTwentyNineSecondClassContainer extends PureComponent {
  render() {
    return (
 	   <ModuleTwentyNineSecondClassView />
    );
  }
}