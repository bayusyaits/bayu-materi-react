function MotorComponent() {
  return (
    <div>
      <h1>Komponen Motor</h1>
    </div>
  );
 }
 
 const MotorGpComponent = () => {
  return (
    <div>
      <h1>Komponen Motor GP</h1>
    </div>
  );
 }
 
 
 const ModuleTwentyNineSecondFunctionView = () => {
  return (
    <div className="App">
      <MotorComponent/>
      <MotorGpComponent/>
    </div>
  );
 }
 
 export default ModuleTwentyNineSecondFunctionView;