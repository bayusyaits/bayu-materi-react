import React from "react";
import ModuleTwentyNineSecondFunctionView from "./ModuleTwentyNineSecondFunctionView";

function ModuleTwentyNineSecondFunctionContainer() {
  return (
      <ModuleTwentyNineSecondFunctionView
      />
  );
}

export default ModuleTwentyNineSecondFunctionContainer;
