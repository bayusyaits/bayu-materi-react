import React from "react";
import ModuleTwentyNineSecondElementView from "./ModuleTwentyNineSecondElementView";

function ModuleTwentyNineSecondElementContainer() {
  return (
      <ModuleTwentyNineSecondElementView
      />
  );
}

export default ModuleTwentyNineSecondElementContainer;
