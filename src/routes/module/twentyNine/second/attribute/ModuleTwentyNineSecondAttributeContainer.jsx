import React from "react";
import ModuleTwentyNineSecondAttributeView from "./ModuleTwentyNineSecondAttributeView";

function ModuleTwentyNineSecondAttributeContainer() {
  return (
      <ModuleTwentyNineSecondAttributeView
      />
  );
}

export default ModuleTwentyNineSecondAttributeContainer;
