const handleChange = (e) => {
  console.log('change', e.target.value)
 }
 const handleBlur = (e) => {
  console.log('blur', e.target.value)
 }
 const handleFocus = (e) => {
  console.log('focus', e.target.value)
 }
 const ModuleTwentyNineSecondAttributeView = () => {
  return (
    <div>
      <input
        type="text"
        placeholder="username"
        className="form-input"
        style={{borderColor: 'red'}}
        onChange={(e) => handleChange(e)}
        onBlur={(e) => handleBlur(e)}
        onFocus={(e) => handleFocus(e)}
      />
    </div>
  );
 };
 export default ModuleTwentyNineSecondAttributeView;