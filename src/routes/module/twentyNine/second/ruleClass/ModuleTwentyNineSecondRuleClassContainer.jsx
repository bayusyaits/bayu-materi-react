import React from "react";
import ModuleTwentyNineSecondRuleClassView from "./ModuleTwentyNineSecondRuleClassView";

function ModuleTwentyNineSecondRuleClassContainer() {
  return (
      <ModuleTwentyNineSecondRuleClassView
      />
  );
}

export default ModuleTwentyNineSecondRuleClassContainer;
