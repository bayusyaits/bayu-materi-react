import React from "react";
import ModuleTwentyNineSecondRuleFunctionView from "./ModuleTwentyNineSecondRuleFunctionView";

function ModuleTwentyNineSecondRuleFunctionContainer() {
  return (
      <ModuleTwentyNineSecondRuleFunctionView
      />
  );
}

export default ModuleTwentyNineSecondRuleFunctionContainer;
