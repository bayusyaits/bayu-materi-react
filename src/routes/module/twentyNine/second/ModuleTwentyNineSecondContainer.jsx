import React from "react";
import { useParams } from "react-router-dom";
import ModuleTwentyNineSecondView from "./ModuleTwentyNineSecondView";
import ModuleTwentyNineSecondFunction from "./function";
import ModuleTwentyNineSecondClass from "./class";
import ModuleTwentyNineSecondElement from "./element";
import ModuleTwentyNineSecondAttribute from "./attribute";
import ModuleTwentyNineSecondRuleClass from "./ruleClass";
import ModuleTwentyNineSecondRuleFunction from "./ruleFunction";

function ModuleTwentyNineSecondContainer() {
  const {
    name
  } = useParams()
  const s = name ? name.toLocaleLowerCase() : ''
  let Component = ModuleTwentyNineSecondView
  if (s) {
    switch (s) {
      case 'function': 
        Component = ModuleTwentyNineSecondFunction
        break;
      case 'class': 
        Component = ModuleTwentyNineSecondClass
        break;
      case 'element': 
        Component = ModuleTwentyNineSecondElement
        break;
      case 'rule-class': 
        Component = ModuleTwentyNineSecondRuleClass
        break; 
      case 'rule-function': 
        Component = ModuleTwentyNineSecondRuleFunction
        break; 
      case 'attribute': 
        Component = ModuleTwentyNineSecondAttribute
        break;
      default: 
        Component = ModuleTwentyNineSecondView
        break;
    }
  }
  return (
      <Component
      />
  );
}

export default ModuleTwentyNineSecondContainer;
