import React from "react";
import ModuleTwentyNineView from "./ModuleTwentyNineView";
import ModuleTwentyNineFirst from "./first";
import ModuleTwentyNineSecond from "./second";
import ModuleTwentyNineThird from "./third";
import ModuleTwentyNineFourth from "./fourth";
import { useParams } from "react-router-dom";

function ModuleTwentyNineContainer() {
  const {
    slug
  } = useParams()
  let Component = ModuleTwentyNineView
  const s = slug ? slug.toLocaleLowerCase() : ''
  if (s) {
    switch (s) {
      case 'first': 
        Component = ModuleTwentyNineFirst
        break;
      case 'second': 
        Component = ModuleTwentyNineSecond
        break; 
      case 'third': 
        Component = ModuleTwentyNineThird
        break; 
      case 'fourth': 
        Component = ModuleTwentyNineFourth
        break; 
      default:
        Component = ModuleTwentyNineView;
        break;
    }
  }
  return (
      <Component
      />
  );
}

export default ModuleTwentyNineContainer;
