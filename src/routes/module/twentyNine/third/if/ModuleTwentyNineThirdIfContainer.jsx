import React from "react";
import ModuleTwentyNineThirdIfView from "./ModuleTwentyNineThirdIfView";

function ModuleTwentyNineThirdIfContainer() {
  return (
    <div> 
      <ModuleTwentyNineThirdIfView />
    </div>); 
}

export default ModuleTwentyNineThirdIfContainer;
