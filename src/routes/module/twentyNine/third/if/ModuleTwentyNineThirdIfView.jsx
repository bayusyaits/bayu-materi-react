import React from 'react'
function LoggedInComponent() {
  return (
    <div>
      <h4>Halo, Selamat Datang Kembali</h4>
    </div>
  );
 }
 function LoginComponent() {
  return (
    <div>
      <h4>Silahkan Login</h4>
    </div>
  );
 }
 function AuthComponent(props) {
   const isLoggedIn = props.isLoggedIn
   if (isLoggedIn) {
    return (<LoggedInComponent />);
   }
   return (<LoginComponent />);
 }
 
function ModuleTwentyNineThirdIfView() { 
  return (<AuthComponent isLoggedIn={true} />);
}
export default ModuleTwentyNineThirdIfView
