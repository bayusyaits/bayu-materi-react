import React from "react";
import ModuleTwentyNineThirdView from "./ModuleTwentyNineThirdView";
import ModuleTwentyNineThirdIf from "./if";
import ModuleTwentyNineThirdIfAnd from "./ifAnd";
import ModuleTwentyNineThirdIfElse from "./ifElse";
import ModuleTwentyNineThirdSwitch from "./switch";
import ModuleTwentyNineThirdPreventing from "./preventing";
import { useParams } from "react-router-dom";

function ModuleTwentyNineThirdContainer() {
  const {
    name
  } = useParams()
  const s = name ? name.toLocaleLowerCase() : ''
  let Component = ModuleTwentyNineThirdView
  if (s) {
    switch (s) {
      case 'if': 
        Component = ModuleTwentyNineThirdIf
        break;
      case 'if-and': 
        Component = ModuleTwentyNineThirdIfAnd
        break;
      case 'if-else': 
        Component = ModuleTwentyNineThirdIfElse
        break;
      case 'switch': 
        Component = ModuleTwentyNineThirdSwitch
        break; 
      case 'preventing': 
        Component = ModuleTwentyNineThirdPreventing
        break; 
      default: 
        Component = ModuleTwentyNineThirdView
        break;
    }
  }

  return (
      <Component
      />
  );
}

export default ModuleTwentyNineThirdContainer;
