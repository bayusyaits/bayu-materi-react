import React from "react";
import ModuleTwentyNineThirdSwitchView from "./ModuleTwentyNineThirdSwitchView";

function ModuleTwentyNineThirdSwitchContainer() {
  return (
    <div> 
      <ModuleTwentyNineThirdSwitchView />
    </div>); 
}

export default ModuleTwentyNineThirdSwitchContainer;
