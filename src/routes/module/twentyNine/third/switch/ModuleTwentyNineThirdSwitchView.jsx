function HeaderComponent(props) {
  switch (props.status) {
   case 'admin':
    return (<header>ADMIN</header>);
   case 'staff':
    return (<header>STAFF</header>);
   default:
    return (<header>GUEST</header>);
  }
 }
 
function ModuleTwentyNineThirdSwitchView() {
 return (
   <div>
     <HeaderComponent status={'admin'}/>
   </div>
 );
}
export default ModuleTwentyNineThirdSwitchView
