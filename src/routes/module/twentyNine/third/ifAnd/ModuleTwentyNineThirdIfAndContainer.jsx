import React from "react";
import ModuleTwentyNineThirdIfAndView from "./ModuleTwentyNineThirdIfAndView";

function ModuleTwentyNineThirdIfAndContainer() {
  return (
    <div> 
      <ModuleTwentyNineThirdIfAndView />
    </div>); 
}

export default ModuleTwentyNineThirdIfAndContainer;
