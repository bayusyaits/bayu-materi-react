import { useState } from "react";
function HeaderComponent(props) {
 if (!props.isLoggedIn) {
   return (<header>Silahkan masuk</header>)
 }
 return (<header>Dashboard</header>)
}
function ModuleTwentyNineThirdIfPreventingView() {
 const [isLoggedIn, setLoggedIn] = useState(false)
 const handleClick = () => {
   setLoggedIn(!isLoggedIn)
 }
 return (
   <div>
     <HeaderComponent
       isLoggedIn={isLoggedIn}
       handleClick={handleClick}/>
     <h5><button
       type="button"
       onClick={() => handleClick()}>
         {isLoggedIn ? 'Logout' : 'Login'}
       </button></h5>
   </div>
 );
}
export default ModuleTwentyNineThirdIfPreventingView
