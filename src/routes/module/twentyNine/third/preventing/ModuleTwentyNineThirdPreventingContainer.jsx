import React from "react";
import ModuleTwentyNineThirdPreventingView from "./ModuleTwentyNineThirdPreventingView";

function ModuleTwentyNineThirdPreventingContainer() {
  return (
    <div> 
      <ModuleTwentyNineThirdPreventingView />
    </div>); 
}

export default ModuleTwentyNineThirdPreventingContainer;
