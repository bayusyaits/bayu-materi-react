import React from "react";
import ModuleTwentyNineThirdIfElseView from "./ModuleTwentyNineThirdIfElseView";

function ModuleTwentyNineThirdIfElseContainer() {
  return (
    <div> 
      <ModuleTwentyNineThirdIfElseView />
    </div>); 
}

export default ModuleTwentyNineThirdIfElseContainer;
