function ModuleTwentyNineThirdIfAndView(props) {
  const isLoggedIn = props.isLoggedIn
  return (
    <header>
      {
        isLoggedIn ? (<div>Logout</div>) : (<div>Login</div>)
      }
    </header>
  );
 } 
export default ModuleTwentyNineThirdIfAndView
