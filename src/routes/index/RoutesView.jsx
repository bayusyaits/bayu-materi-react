import React, { Suspense } from 'react'
import { Routes, Route } from 'react-router-dom'
import Home from '../home'
import NotFound from '../notFound'
import Module from '../module'
import ModuleThirty from '../module/thirty'
import ModuleThirtyOne from '../module/thirtyOne'
import ModuleTwentyNine from '../module/twentyNine'
import ModuleTest from '../module/test'

const PrivateRoute = ({ element }) => {
  return element
}

function RoutesView() {
  return (
    <Suspense>
      <Routes>
        <Route path='/' element={<PrivateRoute element={<Home />}></PrivateRoute>} />
        <Route path="module">
          <Route path="" element={<PrivateRoute element={<Module />}></PrivateRoute>} />
          <Route
            path="thirty"
            element={<PrivateRoute element={<ModuleThirty />}></PrivateRoute>} />
          <Route
            path="thirty/:slug"
            element={<PrivateRoute element={<ModuleThirty />}></PrivateRoute>} />
          <Route
            path="thirty/:slug/:name"
            element={<PrivateRoute element={<ModuleThirty />}></PrivateRoute>} />
          <Route
            path="thirty-one"
            element={<PrivateRoute element={<ModuleThirtyOne />}></PrivateRoute>} />
          <Route
            path="thirty-one/:slug"
            element={<PrivateRoute element={<ModuleThirtyOne />}></PrivateRoute>} />
          <Route
            path="thirty-one/:slug/:name"
            element={<PrivateRoute element={<ModuleThirtyOne />}></PrivateRoute>} />
          <Route
            path="twenty-nine"
            element={<PrivateRoute element={<ModuleTwentyNine />}></PrivateRoute>} />
          <Route
            path="twenty-nine/:slug"
            element={<PrivateRoute element={<ModuleTwentyNine />}></PrivateRoute>} />
          <Route
            path="twenty-nine/:slug/:name"
            element={<PrivateRoute element={<ModuleTwentyNine />}></PrivateRoute>} />
          <Route
            path="test"
            element={<PrivateRoute element={<ModuleTest />}></PrivateRoute>} />
        </Route>
        {/* Not Found */}
        <Route path='*' element={<NotFound />} />
      </Routes>
    </Suspense>
  )
}

export default RoutesView
